const { transport } = require('../config/nodemailer');

const sendEmail = (to, subject, html) => {
    const htmlEmail = `<!DOCTYPE html>
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <header style="padding: 20px 0; border-bottom: 4px solid #8D4896;">
            <img src="https://bancoripley-leonelcarrasco.herokuapp.com/assets/images/LogoRipley.png" style="width: 200px;" alt="">
        </header>
    
        <div>
           ${html} 
           <p>Atte. bancoripley.cl</p>
        </div>
    </body>
    </html>`;

    const message = {
        from: 'notificacion@pruebatecnicaripley.cl',
        to: to,
        subject: subject,
        html: htmlEmail
    };

    transport.sendMail(message, function(err, info) {
        if (err) {
          console.log(err)
        } else {
          console.log(info);
        }
    });
};

const getEmailHtmlDepositOrder = (user, transaction, type) => {
    return `
    <h3 style="color: #8D4896;">Hola ${user.fullname}!</h3>
    <p>Has realizado un ${type} de <b>$${transaction.amount}</b> ${ type == 'depósito' ? 'a' : 'de'} tu cuenta <b>${user.account}</b></p>
    `;
};

const getEmailHtmlTransferFrom = (user, transaction, nameCustomerDestination) => {
    return `
    <h3 style="color: #8D4896;">Hola ${user.fullname}!</h3>
    <p>Has realizado una transferencia de <b>$${transaction.amount}</b> de tu cuenta <b>${user.account}</b> al destinatario <b>${nameCustomerDestination}</b></p>
    `;
};

const getEmailHtmlTransferTo = (user, transaction, nameCustomerFrom) => {
    return `
    <h3 style="color: #8D4896;">Hola ${user.fullname}!</h3>
    <p>Has recibido una transferencia de <b>$${transaction.amount}</b> a tu cuenta <b>${user.account}</b> de <b>${nameCustomerFrom}</b></p>
    `;
};

module.exports = { sendEmail, getEmailHtmlDepositOrder, getEmailHtmlTransferFrom, getEmailHtmlTransferTo }