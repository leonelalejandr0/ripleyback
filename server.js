const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const morgan = require('morgan');
const actuator = require('express-actuator');
const cors = require('cors');

const _constants = require('./config/constants');
const _routes = require('./routes/routes');
const _database = require('./config/database');
const path = require('path');

  
const app = express();
app.use(cors());
app.use(actuator());




app.use(helmet());
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(_constants.INITIAL_PATH, _routes);
app.use(express.static('public'));
  app.get('*',(req,res)=>{
    res.sendFile(path.join(__dirname,'public/index.html'));
});


(async() => {
  const connected = await _database.connect();
  if(connected){
    app.listen(process.env.PORT  || _constants.PORT, () => {
      console.log(`Server connected in ${_constants.PORT}`);
    });
  }
  
})();


module.exports = app;
