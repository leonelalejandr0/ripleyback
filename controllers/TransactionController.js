const HttpStatus = require('http-status-codes');
const moment = require('moment-timezone');

const { Transaction } = require('../models/Transaction.model');
const { User } = require('../models/User.model');
const { sendEmail, getEmailHtmlDepositOrder, getEmailHtmlTransferFrom, getEmailHtmlTransferTo } = require('../utils/sendemail');


const deposit = async(req, res) => {

    const { user, body:data } = req;
    const availableBalance = await getAvailableBalanceByUser(user.rut);
    console.log("SALDO", availableBalance);
    const transaction = {
        customer : user.rut,
        account : user.account,
        transactionType : 'DEPOSITO',
        typeAmount: '+',
        amount : data.amount,
        description : `Se realizó un depósito por $${data.amount}`,
        dateTime : getDate(),
        availableBalance : availableBalance + data.amount
    };
    Transaction.create(transaction)
        .then( ( ) => {
            const html = getEmailHtmlDepositOrder(user, transaction, 'depósito');
            sendEmail(user.email, 'Has realizado un nuevo depósito', html);
            return res.status(HttpStatus.OK).json({
                message : 'Depósito realizado correctamente'
            }) 
        });
};

const order = async(req, res) => {
    const { user, body:data } = req;
    const availableBalance = await getAvailableBalanceByUser(user.rut);
    if(data.amount <= availableBalance){
        const transaction = {
            customer : user.rut,
            account : user.account,
            transactionType : 'RETIRO',
            typeAmount: '-',
            amount : data.amount,
            description : `Se realizó un retiro por $${data.amount}`,
            dateTime : getDate(),
            availableBalance : availableBalance - data.amount
        };
        Transaction.create(transaction)
            .then( () => {
                const html = getEmailHtmlDepositOrder(user, transaction, 'retiro');
                sendEmail(user.email, 'Has realizado un nuevo retiro', html);
                return res.status(HttpStatus.OK).json({
                    message : 'Retiro realizado correctamente'
                });
            });

    }else{
        return res.status(HttpStatus.BAD_REQUEST).json({
            message : 'No puedes retirar esa cantidad'
        });
    }
};

const transfer = async(req, res) => {
    const { user, body:data } = req;
    const availableBalance = await getAvailableBalanceByUser(user.rut);
    if(data.amount <= availableBalance){
        const userDestination = await getUser(data.rut);
        if(userDestination){
            if(userDestination.rut != user.rut){
                const date = getDate();
                const availableBalanceUser = await getAvailableBalanceByUser(user.rut);
                const availableBalanceUserDestination = await getAvailableBalanceByUser(userDestination.rut);
                const transactions = [{
                    customer : user.rut,
                    account : user.account,
                    transactionType : 'TRANSFERENCIA',
                    typeAmount: '-',
                    amount : data.amount,
                    description : `Se realizó una transferencia por $${data.amount} a ${userDestination.fullname}`,
                    dateTime : date,
                    availableBalance : availableBalanceUser - data.amount
                },{
                    customer : userDestination.rut,
                    account : userDestination.account,
                    transactionType : 'TRANSFERENCIA',
                    typeAmount: '+',
                    amount : data.amount,
                    description : `Recibiste una transferencia por $${data.amount} de ${user.fullname}`,
                    dateTime : date,
                    availableBalance : availableBalanceUserDestination + data.amount
                }]

                Transaction.insertMany(transactions)
                    .then( () => {
                        const htmlEmailFrom = getEmailHtmlTransferFrom(user, transactions[0], userDestination.fullname);
                        const htmlEmailTo = getEmailHtmlTransferTo(userDestination, transactions[0], user.fullname);
                        sendEmail(user.email, 'Has realizado una nueva transferencia', htmlEmailFrom);
                        sendEmail(userDestination.email, 'Has recibido una transferencia', htmlEmailTo);
                        return res.status(HttpStatus.OK).json({
                            message : 'Transferencia realizada correctamente'
                        })
                    });

            }else{
                return res.status(HttpStatus.BAD_REQUEST).json({
                    message : 'No puedes transferir a tu misma cuenta'
                });
            }
        }else{
            return res.status(HttpStatus.NOT_FOUND).json({
                message : 'Cuenta destino no existe'
            });  
        }
    }else{
        return res.status(HttpStatus.BAD_REQUEST).json({
            message : 'No puedes transferir esa cantidad'
        });
    }
};

const transactions = (req, res) => {
    
    const {user} = req;

    Transaction.find({
        customer : user.rut
    })
    .sort({
        dateTime : -1
    })
    .then( async( results ) => {
        return res.status(HttpStatus.OK).json({
            transactions : results,
            totalRows : results.length,
            availableBalance : await getAvailableBalanceByUser(user.rut)
        });
    });

};

const getDate = () => {
    return moment.tz('America/Santiago').format();
};

const getAvailableBalanceByUser = async(rut) => {
    const availableBalance = await Transaction.findOne({
        customer : rut
    }, 'availableBalance -_id').sort({
        dateTime : -1
    }).then( (result) => {
        if( !result )
            return 0;
        return result.availableBalance;
    });
    return availableBalance;
};

const getUser = async(rut) => {
    const user = await User.findOne({
        rut : rut
    }).then( (result) => {
        return result;
    });
    return user;
};

module.exports = { deposit, order, transfer, transactions }