const HttpStatus = require('http-status-codes');
const jwt = require('jsonwebtoken');

const _constants = require('../config/constants');
const { User } = require('../models/User.model');

const login = (req, res) => {
    const {body:credentials} = req;
    User.findOne(credentials)
        .then( ( result ) => {
            if( !result )
                return res.status(HttpStatus.UNAUTHORIZED).json({
                    message : 'Los datos rut y/o contraseña son incorrectos'
                });
            
            const jwtStr = jwt.sign({
                data: JSON.stringify(result)
              }, _constants.SECRET_JWT, { expiresIn: '1h' });
            return res.status(HttpStatus.OK).json({
                token : jwtStr
            });
        });
};

const register = (req, res) => {
    const {body:user} = req;
    user.account = user.rut.split("-")[0];
    User.create( user )
        .then( ( ) => res.status(HttpStatus.CREATED).json({
            message : 'Usuario registrado correctamente'
        }))
        .catch( ( err ) => {
            if(err.code == 11000)
                return res.status(HttpStatus.BAD_REQUEST).json({
                    message : 'El usuario ya existe'
                });
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                message : 'Error desconocido'
            });
        });
};

const getUser = (req, res) => {
    const { user } = req;
    delete user._id;
    delete user.password;
    return res.status(HttpStatus.OK).json(user);
};

module.exports = { login, register, getUser }