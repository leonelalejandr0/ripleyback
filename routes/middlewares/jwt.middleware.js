const jwt = require('jsonwebtoken');
const HttpStatus = require('http-status-codes');
const _constants = require('../../config/constants');

const validateJWT = (req, res, next) => {
    const token = req.headers['token'];
    if(!token){
        res.status(HttpStatus.FORBIDDEN).json({
            message : 'Es necesario el token de autenticación'
        });
        return
    }

    jwt.verify(token, _constants.SECRET_JWT, function(err, decode) {
        if (err) {
            res.status(HttpStatus.FORBIDDEN).json({
                message : 'No es un token válido'
            });
        } else {
            req.user = JSON.parse(decode.data);
            next();
        }
    })

};

module.exports = { validateJWT }
