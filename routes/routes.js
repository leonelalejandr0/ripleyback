const express = require('express');

const { login, register, getUser } = require('../controllers/AuthController');
const { deposit, order, transfer, transactions } = require('../controllers/TransactionController');

const { validateJWT } = require('./middlewares/jwt.middleware');

const router = express.Router();
router.post('/auth/login', login);
router.post('/auth/register', register);
router.get('/auth/me', validateJWT, getUser);

router.post('/transactions/order', validateJWT, order);
router.post('/transactions/deposit', validateJWT, deposit);
router.post('/transactions/transfer', validateJWT, transfer);
router.get('/transactions/list', validateJWT, transactions)

module.exports = router;