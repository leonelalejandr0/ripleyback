# MiniBancoBack

Proyecto MiniBanco desarrollado con NodeJS/Express

## Requisitos

- [NodeJS](https://nodejs.org/es/download/)

## Instalación

```bash
git clone git@gitlab.com:leonelalejandr0/ripleyback.git
```

Deben clonar el proyecto junto donde está el frontend [MiniBancoFront](https://gitlab.com/leonelalejandr0/ripleyfront). Una vez hecho lo anterior, en la terminal ejecutar el siguiente comando para instalar las dependencias

```bash
npm install
```

## Uso

Al terminar de instalar las dependencias,  podemos levantar el proyecto con el siguiente comando

```bash
npm start
```

Iniciado el servicio, se debe abrir la siguiente URL: 
http://localhost:3000


## Licencia
Desarrollado por Leonel Carrasco
