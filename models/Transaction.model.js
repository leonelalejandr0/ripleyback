const mongoose = require('mongoose');

const transactionSchema = new mongoose.Schema({
  customer : String,
  account : String,
  transactionType : String,
  typeAmount:String,
  amount: Number,
  description: String,
  dateTime: Date,
  availableBalance: Number
},{
  versionKey : false,
});



const Transaction = mongoose.model('Transaction', transactionSchema);

module.exports = { Transaction };