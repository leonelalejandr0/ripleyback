const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  rut: {
    type : String,
    unique : true
  },
  fullname : String,
  email : String,
  password: String,
  account: String
},{
  versionKey : false,
});



const User = mongoose.model('User', userSchema);

module.exports = { User };